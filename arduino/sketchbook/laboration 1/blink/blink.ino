/*
 * TNK116 - Internet of Things
 * Sketch for blink
 * Board Arduino UNO
 * 1 x green LED
 */

//Selecting the pins for the LED (green)
const int LED_GREEN = 13;

void setup() {
  // Setup for that runs once in the beginning.
  // Defining the type of pins, in this case output.
  pinMode(LED_GREEN, OUTPUT);
}

void loop() 
{
  // This code will be run forever as a loop.

  // Setting the green and red pin to high so current can flow.
  digitalWrite(LED_GREEN, HIGH);
  
  // Waiting 1 second.
  delay(1000);
  
  // Setting the green and red pin to low so no current can flow.
  digitalWrite(LED_GREEN, LOW);

  // Waiting 1 second.
  delay(1000);
}
