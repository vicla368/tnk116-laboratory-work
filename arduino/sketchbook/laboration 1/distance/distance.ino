/*
 * TNK116 - Internet of Things
 * Sketch for Distance sensing and LED actuation
 * Board Arduino UNO
 * 1 x ultrasonic distance sensor HC-SR04 
 * 1 x green LED
 * 1 x red LED
 */

// Setting the pins for the sensor and the LEDs.
// TODO: Set the pins according to your build.
const int trigPin = 
const int echoPin =
const int LED_RED =
const int LED_GREEN =

// Setting variables.
float duration, distance;

// Setup for that runs once in the beginning.
// Defining the type of pins and starting a serial connection.
void setup() {
  // TODO: Define the type of pin for the 4 pins.
  Serial.begin(9600);
}

// This code will be run forever as a loop.
void loop() {
  //Setting initial paramaters for the HC-SR04 pins
  digitalWrite(trigPin, LOW);
  
  // Delaying the code and then sends a trigger pulse.
  delayMicroseconds(2);
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(2);
  digitalWrite(trigPin, LOW);

  // Reading the measurements from the HC-SR04.
  duration = pulseIn(echoPin, HIGH);
  
  // Calculating the distance in cm.
  // TODO: Read the specifications of the HC-SR04 to learn how calculate the 
  // distance from the duration. The specifications are found on Lisam.
  distance = 
  
  // Lights up the red LED if the distance is shorter than 10 cm.
  if (distance <=10){
    digitalWrite(LED_GREEN,LOW);
    digitalWrite(LED_RED, HIGH);
  }
  // Lights up the green LED if the distance is longer than 10 cm.
  else {
    digitalWrite(LED_GREEN,HIGH);
    digitalWrite(LED_RED, LOW);
  }
  
  // Printing the distance on the serial port
  Serial.print("Distance: ");
  Serial.println(distance);
  
  // Delaying the code for 100 ms before the loop restarts.
  delay(100);
}
