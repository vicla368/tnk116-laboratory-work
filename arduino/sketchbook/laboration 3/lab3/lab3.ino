/*
 * TNK116 - Internet of Things
 * Sketch for blink
 * Board Arduino UNO
 * 1 x green LED
 * 1 x red LED
 * 1 x DHT22 (Temperature/Humidity sensor)
 */

// Including 3rd party software for reading DHT22.
#include "DHT.h"
#include "Adafruit_Sensor.h"

// Declaring pin and sensor type.
// Todo: Change the "PIN_NR" to pin number for the input pin of the 
//       temperature sensor

#define DHTPIN 11
#define DHTTYPE DHT22

// Declaring a DHT object. 
DHT dht(DHTPIN, DHTTYPE);

const int LED_GREEN = 12;
const int LED_RED = 13;
// Declaring variable to store LED instructions.
int type = NULL;
int length = NULL;

// Selecting the pins for different purpose.

int measuringInterval = 0;
int measuringCounter = 0;

int redLedDuration = 0;


// This method sends measurements to the Raspberry pi over the serial 
// connection. The data is sent in a TLV structure. [Type][Length][Value].
// The size of the type field is 1 byte.
// The size of the length field is 2 bytes.
// The size of the value field varies.


void sendData(int type, String value)
{
	// Printing the type.
	Serial.print(type);
	
	// Printing the length.
   int length = value.length();
//    int length = sizeof(value);
	if(length < 10)
	{
		Serial.print("0");
	}
	
	Serial.print(length);
	
	// Printing the value.
	Serial.print(value);
 
}

// This method handles messages received from the Raspberry Pi.
// The type is used for identifying the type of message.
void handleMessage(int type, byte value[])
{
	// Setting the interval of measurement readings.
	if(type == 1) // Todo: Set this type number to match the gateway code.
	{
		// Reads the values of the message.
		byte parsedValue[length+1];
		for(int i = 0 ; i < length ; i++)
		{
			parsedValue[i] = value[i];
		}
		
		// Adds a \0 to make the ascii-to-integer code to work.
		parsedValue[length] = '\0';
		
		// Saving the measurement insterval by converting ascii text to integer.
		measuringInterval = atoi(parsedValue);
	}
  if(type == 2) // turn on red led for value seconds
  {
    byte parsedValue[length+1];
    for(int i = 0 ; i < length ; i++)
    {
      parsedValue[i] = value[i];
    }
    
    // Adds a \0 to make the ascii-to-integer code to work.
    parsedValue[length] = '\0';
    
    // Saving the measurement insterval by converting ascii text to integer.
    redLedDuration = atoi(parsedValue);
  }
}

// This method reads data from the serial connection.
// The method extracts the type and length and sends the message to the handler
// when all bytes have arrived.
void receiveData()
{
  // Checks if no message is in the pipe.
  if(type == NULL)
  {
	  // Checks if all bytes for the type and length has arrived.
	  if(Serial.available()>=3)
	  {
		  // Creates a byte array and reads the type.
		  byte typeBuffer[2];
		  Serial.readBytes(typeBuffer,1);
		  
		  // Adds a \0 to make the ascii-to-integer code to work.
		  typeBuffer[1] = '\0';
		  
		  // Saves the type as a integer.
		  type = atoi(typeBuffer);
		  
		  
		  // Creates a byte array and reads the length.
		  byte lengthBuffer[3];
		  Serial.readBytes(lengthBuffer,2);
		  
		  // Adds a \0 to make the ascii-to-integer code to work.
		  typeBuffer[2] = '\0';
		  
		  // Saves the length as a integer.
		  length = atoi(lengthBuffer);
	  }
  }

  // Checks if a message is in the pipe.
  if(type != NULL)
  {
	  // Checks if all bytes for the message has arrived.
	  if(Serial.available()>= length)
	  {
		  // Creates a byte array and reads the type.
		  byte valueBuffer[length];  		  
		  Serial.readBytes(valueBuffer,length);
		  
		  // Handles the message.
		  handleMessage(type, valueBuffer);
		  
		  // Setting the message as handled by setting the type to NULL.
		  type = NULL;
	  }
  }
}
	  
  
void setup() {
  // Setup for that runs once in the beginning.
  // Defining the type of pins, in this case output.
  
  // Initializing the DHT sensor.
  dht.begin();
  
  // Starting a serial connection.
  Serial.begin(9600);

  pinMode(LED_GREEN, OUTPUT);
  pinMode(LED_RED, OUTPUT);
}

// This is the main loop of the Arduino code. 
void loop() 
{
  receiveData();
  
  // Collecting mesurements.
  if(measuringInterval > 0)
  {
    digitalWrite(LED_GREEN, HIGH);
	  // Checking if the measurement shall be collected.
	  measuringCounter = measuringCounter % measuringInterval;
	  if(measuringCounter == 0)
	  {

		  // Reading the temperature from the sensor.
		  float temperature = dht.readTemperature();
		  if (!isnan(temperature))
		  {		  
			  // Creating a string representation of the temperature.
			  String output = "";
			  char temperatureChar[6];
			  dtostrf(temperature,2,2,temperatureChar);
			  for(int i = 0 ; i < 6; i++)
			  {
				  output+=temperatureChar[i];
			  }
			  
			  // Sending the data to the Raspberry Pi.
			  // Todo: Set this type number to match the gateway code.
			  sendData(1,output);

			  measuringCounter = measuringCounter + 1;
		  }
	  }
	  else
	  {
		  measuringCounter = measuringCounter + 1;
	  }
  }
  else
  {
    digitalWrite(LED_GREEN, LOW);
  }

  if(redLedDuration > 0)
  {
    digitalWrite(LED_RED,HIGH);
    redLedDuration--;
  }else{
    digitalWrite(LED_RED, LOW);
  }
  // Waiting 1 second.
  delay(1000);
}
