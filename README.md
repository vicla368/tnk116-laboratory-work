# TNK116 Course material
This repository contains the source code for the different labrotary tasks in TNK116 IoT.


## Usage
1. Fork or clone this repository and download it to your computer.
2. The files located in the arduino folder contains source code, libraries and installation instructions for the arduino code. 
3. The files located in the pi folder contains source code and programs used on the Raspberry Pi.