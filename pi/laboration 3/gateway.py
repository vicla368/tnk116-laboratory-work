#!/usr/bin/python3

## This program is designed to run on the Raspberry Pi.
## The program is used as an gateway for the Arduino in the laboratory work
## assignment.
##
## Tip: There is one method called output built in the code to make it easier
##      to print out variables or messages.
##      Example  output("Test!") - Prints the text Test! in the output area.
##

from colorama import Fore, Back, Cursor, Style, init
import select
import random
import sys
import time
import os
import serial
import socket
import json
import textwrap
from datetime import datetime

##############################################
##                                          ##
##                Variables                 ##
##                                          ##
##############################################
# Network variables.
unit_id =  16 # Todo: Set the unit id here.
host = '130.236.81.13'
port = 8718

# Serial connection variables.
ser = serial.Serial('/dev/ttyACM0',9600)

#Input command variable
command = None

# Output print row.
outputRow = 0

# Update variables
updateFrequency=1 # seconds.

# Task list
taskList = []

# Arduino reading variables
messageType = None
length = None

temperature = None
##############################################
##                                          ##
##              Main function               ##
##                                          ##
##############################################

# This is the main method that loops forever.
def main():
    init()
    # Printing menu.
    printMenu()
    # Printing output section.
    printOutputSection()
    while True:
            try:
                # Checking for new service requests.
                loadServiceRequests()
		# Reading response from the Arduino.
                readFromArduino()
				
		# Handle service requests.
                handleServiceRequests()
				
		# Handles input and waits.
                getInput()
            except (KeyboardInterrupt, SystemExit):
                raise
            except Exception as e:
                time.sleep(1)
                output(e)
    # Clearing the screen.
    os.system('clear')



# This method query the server for new service requets.
def loadServiceRequests():
    global taskList
    
    if unit_id is None:
        output("Unit id is not set.")
        return

    # Creating a request for new services.
    request = {"message_type":"TNK116Lab3GetTasks","unit_id":unit_id}
    
    # Passes the request to a method that handes server connections.
    response = sendToServer(request)

    # Stops the method if no new service request was found.
    if response["tasks"] is None or len(response["tasks"]) == 0:
        return

    # Adding all new service requests to the task list.
    tasks = response["tasks"]
    for task in tasks:
        outputString = "Service request recived {} from user {}".format(
                        task["service_id"],task["from"])

        output(outputString)

        task["completed"] = False
    
        taskList.append(task)

##############################################
##                                          ##
##        Functions for assignment 3        ##
##                                          ##
##############################################


# Returns true if the requesting user has permission to use the service
def checkPermission(task):

    request = {"message_type":"TNK116Lab3CheckPermission",
               "user_id":task["from"],
               "service_id":task["service_id"]}

    output("sending {}".format(request))
    response = sendToServer(request)
    output("response: {}".format(response))
    if not response["message_type"] == "TNK116Lab3CheckPermissionResponse":
        raise Exception("invalid responce")

    return response["permission"]




# This method handles the tasks in the task list.
# The tasks can either be one time actions or tasks that
# persist over a longer period of time.
def handleServiceRequests():
    global taskList
    global temperature

    # Looping though all taks in the list.
    for task in taskList:
#        output("has permission {}".format(checkPermission(task)))
        if not checkPermission(task):
            output("handeling invalid permission ")
            task["data"] = {"ERRROR":"premision denied"}
            request = {"message_type":"TNK116Lab3RequestService", "user_id":task["from"],"service_id":task["service_id"],"data":task["data"],"response":True}
            # Turn on the red light for 10s
            sendInstructions(2,10)
            task["completed"] = True
        elif task["service_id"] == 24: # Todo: Change None to the service id in the database.
            # Creates a parameter dictionary that stores all relevant information for the service.
            if "parameters" not in task:
                try:
                    # Creates a new dictionary in task called parameters.
                    task["parameters"] = {}

                    # Populates the parameters dictionary with the time interval and the end time parameter.
                    task["parameters"]["interval"] = int(task["data"]["interval"])
                    task["parameters"]["endtime"] = datetime.strptime(task["data"]["endtime"], '%Y-%m-%d %H:%M:%S')

                    # Initializes the parameter last updated.
                    task["parameters"]["last_updated"] = datetime.now()

                    # Sends the instructions to the Arduino.
                    sendInstructions(1,task["parameters"]["interval"]) # Todo: Change the None a more suitable id such as 1 or the service_id.

                    # Overwrites the data dictionary with a new empty dicitonary.
                    task["data"] = {}
                    
                # Sending errors to the output.
                except Exception as e:
                    output("ERROR: {}".format(e))
                    task["completed"] = True
                    
            # Stores the temperature.
            if temperature is not None:
                task["data"] = {}
                task["data"]["temperature"] = temperature
                task["data"]["time"] = datetime.now().strftime(
                    '%Y-%m-%d %H:%M:%S')
                temperature = None

            # Sets the task as completed when the time is later than the end time and sends stop instructions to the Arduino.
            if ("time" in task["data"] and
                datetime.now() >= task["parameters"]["endtime"]):
                task["completed"] = True
                sendInstructions(1,0) #Todo: Change the None to the same type id as line 151.

            # Checks if the difference between the last updated measurement and the current measurement is
            # longer than the interval and if so sends it to the user that requested the data.
            elif ("time" in task["data"] and (
                datetime.strptime(task["data"]["time"], '%Y-%m-%d %H:%M:%S')
                -task["parameters"]["last_updated"]
                ).total_seconds() >= task["parameters"]["interval"]):

                # Sends the request to the server.
                request = {"message_type":"TNK116Lab3RequestService",
                           "user_id":task["from"],
                           "service_id":task["service_id"],
                           "data":task["data"],"response":True}
                response = sendToServer(request)

                # Updates the last updated parameter.
                task["parameters"]["last_updated"] = datetime.strptime(task["data"]["time"], '%Y-%m-%d %H:%M:%S')

        # Checks if the task is completed.
        if task["completed"]:

            # Sends the response to the user.
            request = {"message_type":"TNK116Lab3RequestService","user_id":task["from"],
               "service_id":task["service_id"], "data":task["data"],"response":True}
            response = sendToServer(request)
            outputString = "Service {} is completed for user {}".format(
                        task["service_id"],task["from"])
            output(outputString)

            # Removes the task
            taskList.remove(task)
    temperature = None
        
# This method handles connections to the server and parses the response 
# as a JSON dictionary.
def sendToServer(request):
    # Creating a socket and connects to the set host and port.
    clientsocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)   
    clientsocket.connect((host, port))    
    
    # Dumps the request dictionary as a JSON object.
    # Sending the JSON message to the host. Note: The message must end with a | 
    # character and the String should be encoded to bytes with .encode().
    clientsocket.send("{}|".format(json.dumps(request)).encode())    
    
    # Reading input from the host.
    receive = clientsocket.recv(1024)
    
    # Closing the connection to the server.
    clientsocket.close()                   

    # Creates a string from the receive bytes, removes the trailing | and parses
    # the string as a JSON dictionary.
    # Note the | must be removed from the string before the string is 
    # loaded as a JSON dictionary.
    response = json.loads(str(receive,'utf-8').replace("|",""))

    return response

# This method reads data from the Arduino.
def readFromArduino():
    global messageType
    global length

    # Checks if no message is in the pipe.
    if messageType is None:
        # Checks if all bytes for the type and length has arrived.
        if ser.inWaiting() >= 3:
            # Reads the messageType (first byte) and the message length (next two bytes) and stores them as integers.
            message = ser.read(3)
            output("Receiving header {}".format(message))
            try:
                messageType = int(str(message[0:1],'utf-8'))
                length = int(str(message[1:3],'utf-8'))
                output("Receiving header {}{}".format(messageType,length)) # Can be used for debugging.
            except:
                messageType = None
                raise
    # Checks if a message is in the pipe.
    if messageType is not None:
        # Checks if all bytes for the message has arrived.
        if ser.inWaiting() >= length:
            # Reads the message.
            message = ser.read(length)
            output("Receiving {}".format(str(message,'utf-8'))) # Can be used for debugging.
            # Sends the message to the response handler as a string with extra null bytes removed.
            handleArduinoResponse(messageType, length, str(message,'utf-8').rstrip('\x00'))

            # Setting the message as handled by setting the message type and length to None.
            messageType = None
            length = None


# This method handles responses from the Arduino and do stuff with it.
def handleArduinoResponse(messageType, length, message):
    global temperature
    # Handles messages of type temperature collection.
    if messageType == 1: # Todo: Change the None a more suitable id such as 1.
        # Sets the global temperature variable to the received message.
        output('reading temperature from arduino.')
        temperature = float(message[0:length])
        
    
# This method sends measurements to the Arduino over the serial 
# connection. The data is sent in a TLV structure. [Type][Length][Value].
# The size of the type field is 1 byte.
# The size of the length field is 2 bytes.
# The size of the value field varies.
def sendInstructions(messageType, value):
    value = str(value)
#    output("Sending: {}{:02}{}".format(messageType,len(value),value)) # Useful for debugging.
    # Sends the TLV message to the Arduino as a string.
    ser.write("{}{:02}{}".format(messageType,len(value),value).encode())


##############################################
##                                          ##
##              Input functions             ##
##                                          ##
##############################################

# This method reads the command from from the standard input (keyboard)
def getInput():
    global command

    # Printing menu choise question.
    if command is None:
        printMenuSelector(0)
        command = ""
    # Handling menu choises.
    if '\n' in command:
        choise = command.strip()
        
        # Handling setting of user id.
        if choise == '1':
            # Clearing the screen.
            os.system('clear')
            
            printMenuSelector("Exiting...\n")
            sys.exit()
        
        command = None
        return
    
    # Waiting 1 secod for new commands.
    readReady, writeReady, exceptReady = select.select([sys.stdin], [], [],1)
    
    # Appending new commands to the command variable
    if len(readReady):
        line = sys.stdin.readline()
        command = command+line
        return


##############################################
##                                          ##
##            Graphical functions           ##
##                                          ##
##############################################

def printMenu():
    # Clearing the screen.
    os.system('clear')
    
    # Printing the header
    sys.stdout.write('{message:#<33}\n'.format(message=""))
    sys.stdout.write('#{}#\n'.format("Gateway".center(31," ")))
    sys.stdout.write('{message:#<33}\n'.format(message=""))
    sys.stdout.write('#{message: <31}#\n'.format(message="Unit id: {}".format(unit_id)))
    sys.stdout.write('{message:#<33}\n'.format(message=""))
    
    # Creating the menu.
    menu = {}
    menu['1'] = "Exit."
    
    # Printing the menu
    menuItems=menu.keys()
    for menuItem in menuItems:
        sys.stdout.write("#{message: <31}#\n".format(message="{}. {}"
                         .format(menuItem,menu[menuItem])))
    #sys.stdout.write("#{message: <31}#\n".format(message=""))
    sys.stdout.write("{message:#<33}\n".format(message=""))
    sys.stdout.flush()
    
def printOutputSection():
        # Storing cursor position.
        sys.stdout.write("\033[s")
        
        # Moving to temperature position.
        # Changing color depending on threshhold.
        sys.stdout.write("\033[12;1H")

        sys.stdout.write('{message:#<80}\n'.format(message=""))
        sys.stdout.write('#{}#\n'.format("Output".center(78," ")))
        sys.stdout.write('{message:#<80}\n'.format(message=""))
        for i in range(1,16):
            sys.stdout.write('#{}#\n'.format("".center(78," ")))
        sys.stdout.write('{message:#<80}\n'.format(message=""))
        # Restoring the cursor position.
        sys.stdout.write("\033[u")
        sys.stdout.flush()

def printMenuSelector(question):
    # Moving to the right spot.
    sys.stdout.write("\033[8;0H")
    
    # Clearing row.
    sys.stdout.write("\033[K")
    if question == 0:
        question = "Please select: "
    
    # Printing question.
    sys.stdout.write(question)
    
    # Moving cursor.
    sys.stdout.flush()

def output(text):
    global outputRow

    lines = lines = textwrap.wrap(str(text).strip(), 78, break_long_words=False)
    
    # Storing cursor position.
    sys.stdout.write("\033[s")
    
    # Writing debug
    for line in lines:
        # Moving cursor.
        sys.stdout.write("\033[{};1H".format(15+outputRow))
    
        # Clearing row.
        sys.stdout.write("\033[K")
    
        sys.stdout.write("#{message: <78}#\n".format(message="{}"
                             .format(line.strip())))
        outputRow = (outputRow + 1) % 15
    
    # Restoring the cursor position.
    sys.stdout.write("\033[u")
    sys.stdout.flush()

if __name__=="__main__":
    main()
